Feature: Search functionalities

  Background: Launch the application
    Given open the application

  Scenario: User search the item
    When enter the text "Transparency"
    Then close the application

  Scenario Outline: User navigates Government activity links
    When user click on menu
    And user choose the governmentActivity "<government_activity>"
    Then validate the title "<pageTitle>"
    Then close the application
    Examples:
      | government_activity | pageTitle                                        |
      | Departments         | Departments, agencies and public bodies - GOV.UK |
      | News                | News and communications - GOV.UK1                |