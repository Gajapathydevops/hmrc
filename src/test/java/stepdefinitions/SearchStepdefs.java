package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.LoginPage;
import pages.SearchPage;

public class SearchStepdefs extends SearchPage {


    @When("^enter the text (.+)$")
    public void enterTheText(String searchTerm) {
        searchItem(searchTerm);
    }

    @When("user click on menu")
    public void userClickOnMenu() {
        clickMenu();
    }

    @And("user choose the governmentActivity {string}")
    public void userChooseTheGovernmentActivity(String link) throws InterruptedException {
        clickGovernmentActivity(link);
    }


    @Then("validate the title {string}")
    public void validateTheTitle(String title) {
        validatePageTitle(title);
    }
}
