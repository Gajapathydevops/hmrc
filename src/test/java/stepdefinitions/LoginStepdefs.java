package stepdefinitions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.LoginPage;

import java.net.MalformedURLException;

public class LoginStepdefs extends LoginPage {
    String url = "https://www.gov.uk/log-in-register-hmrc-online-services";

    @Given("open the application")
    public void open_the_application() throws MalformedURLException {
        setup("local", "chrome");
    }

    @Then("close the application")
    public void closeTheApplication() {
        driver.quit();
    }

    @When("click on sing in button")
    public void clickOnSingInButton() {
        clickSignIn();
    }

    @And("enter the {string} and {string}")
    public void enterTheSignInDetails(String gid, String pwd) {
        enterSignDetails(gid, pwd);
        signInSubmit();
    }

    @Then("validate the error message")
    public void validateTheErrorMessage() throws InterruptedException {
        validateErrorMessage();
    }

    @When("perform collection LIST examples")
    public void performCollectionListExamples() {
        collectionListExample();
    }

    @When("perform collection SET examples")
    public void performCollectionSetExamples() {
        collectionSetExample();
    }

    @When("perform collection MAP examples")
    public void performCollectionMapExamples() {
        collectionMapExample();
    }
}
