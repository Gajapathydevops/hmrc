package pages;

public enum Environment {
    PROD("https://www.gov.uk/"),

    DEV("https://www.onlinesbi.sbi/");

    private final String url;

    Environment(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
