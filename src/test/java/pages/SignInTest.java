package pages;

import io.cucumber.java.eo.Se;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SignInTest extends BasePage {
    @Test(priority = 2)
    public void hello() {
        Assert.assertEquals("State Bank of India", driver.getTitle());
        //WebDriverWait waits= new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement element = driver.findElement(By.cssSelector("button[data-toggle='dropdown']"));
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(d -> element.isDisplayed());
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        //LIST EXAMPLE
        List<WebElement> listOptions = driver.findElements(By.cssSelector("ul[class='dropdown-menu inner '] li a"));
        List<String> listOptionsTexts = new ArrayList<>();

        //FOR-EACH
        for (WebElement ele : listOptions) {
            listOptionsTexts.add(ele.getText());
        }
        System.out.println(listOptionsTexts.size());

        //FOREACH
        listOptions.forEach(optionNames -> listOptionsTexts.add(optionNames.getText()));
        System.out.println(listOptionsTexts.size());

        for (String printText : listOptionsTexts) {
            System.out.println("LIST VALUES " + printText);
        }

        //SET EXAMPLE
        Set<String> noDuplicate = new HashSet<>();
        listOptions.forEach(q -> noDuplicate.add(q.getText()));
        for (String printTexts : noDuplicate) {
            System.out.println("SET VALUES " + printTexts);
        }
        String originalWindow = driver.getWindowHandle();
        System.out.println("ORIGINAL WINDOW " + originalWindow);
        System.out.println("ORIGINAL WINDOW URL" + driver.getCurrentUrl());

        // Alert Example
        WebElement popUpLink = driver.findElement(By.cssSelector("a[data-target='#newUserModal']"));
        wait.until(d -> popUpLink.isDisplayed());
        popUpLink.click();
        WebElement OK = driver.findElement(By.cssSelector("a[aria-label='OK']"));
        OK.click();

        //wait.until(ExpectedConditions.alertIsPresent());
//        Alert alert = driver.switchTo().alert();
//        alert.accept();

        // WINDOW HANDLES
        Set<String> windows = driver.getWindowHandles();
        for (String multipleWindows : windows) {
            if (!multipleWindows.equals(originalWindow)) {
                System.out.println("NEW WINDOW " + multipleWindows);
                driver.switchTo().window(multipleWindows);
                System.out.println("NEW WINDOW URL" + driver.getCurrentUrl());
                WebElement NEXT = driver.findElement(By.id("nextStep"));
                NEXT.click();
                driver.close();
            }
        }
        driver.switchTo().window(originalWindow);
        WebElement elementorigina = driver.findElement(By.className("cache_blink"));
        System.out.println("TEXT" + elementorigina.getText());


//        Select select = new Select(driver.findElement(By.xpath("//div[@class=\"dropdown bootstrap-select custom-select bs3\"]/select")));
//        List<WebElement> OPTIONS = select.getOptions();


//        WebElement elementS = driver.findElement(By.className("corp_login"));
//        elementS.click();
    }
}
