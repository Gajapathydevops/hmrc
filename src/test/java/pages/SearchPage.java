package pages;

import org.hamcrest.core.StringContains;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SearchPage extends BaseClass {
    By searchMenu = By.id("super-search-menu-toggle");
    By searchBar = By.cssSelector("input[name=q]");
    By searchButton = By.xpath("//button[@class='gem-c-search__submit']");
    By menu = getLocator(LocatorType.ID, "super-navigation-menu-toggle");
    String governmentActivity = "//ul[contains(@class,'government-activity')]//a[@data-track-dimension='LINK']";


    public void clickMenu() {
        driver.findElement(menu).click();
    }

    public void clickGovernmentActivity(String linkText) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(governmentActivity.toString().replace("LINK", linkText)));
        element.click();
    }

    public void searchItem(String searchTerm) {
        driver.findElement(searchMenu).click();
        driver.findElement(searchBar).sendKeys(searchTerm);
        mouseHoverAndClickElement(searchButton);
    }

    public void validatePageTitle(String title) {
        String text = driver.getTitle();
        System.out.println("Page title" + text);
        Assert.assertTrue(text.equals(title));
    }
}
