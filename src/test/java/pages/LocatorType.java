package pages;

public enum LocatorType {
    ID,
    NAME,
    CLASS_NAME,
    TAG_NAME,
    CSS_SELECTOR,
    XPATH
}