package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.manager.SeleniumManager;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

public class BaseClass {
    public static WebDriver driver;
    static ChromeOptions chromeOptions = new ChromeOptions();
    static FirefoxOptions firefoxOptions = new FirefoxOptions();
    static String remoteUrl = "http://localhost:4444/wd/hub";
    static By acceptCookies = By.cssSelector("button[data-track-action='Cookie banner accepted']");
    static By accept = By.cssSelector("button[data-webviewid='accept-cookies']");

    public static void
    launchBrowser() {
        driver.get(Environment.PROD.getUrl());
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
        clickAcceptCookiesButton();
    }

    public static void browserOption(String browser) {
        if (browser.equalsIgnoreCase("chrome")) {
            //WebDriverManager.chromedriver().setup();
            // chromeOptions.addArguments("--headless");
            //System.out.println("DRIVER PATH--------->" + SeleniumManager.getInstance().getDriverPath(chromeOptions, true));
            driver = new ChromeDriver(chromeOptions);
        } else if (browser.equalsIgnoreCase("firefox")) {
            // firefoxOptions.addArguments("--headless");
            driver = new FirefoxDriver(firefoxOptions);
        }
    }


//    public static WebDriver browserOption1(BrowserType browserType) {
//        switch (browserType) {
//            case CHROME:
//                //chromeOptions.addArguments("--headless");
//                driver = new ChromeDriver(chromeOptions);
//                // driver.get(Environment.PROD.getUrl());
//            case FIREFOX:
//                firefoxOptions.addArguments("--headless");
//                driver = new FirefoxDriver(firefoxOptions);
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + browserType);
//        }
//        return driver;
//    }

    public static void
    setup(String mode, String browser) throws MalformedURLException {
        if (mode.equals("local")) {
            browserOption(browser);
            launchBrowser();
        } else if (mode.equals("remote")) {
            switch (browser) {
                case "chrome":
                    browserOption(browser);
                    driver = new RemoteWebDriver(new URL(remoteUrl), chromeOptions);
                    launchBrowser();
                    break;
                case "firefox":
                    browserOption(browser);
                    driver = new RemoteWebDriver(new URL(remoteUrl), firefoxOptions);
                    launchBrowser();
                    break;
            }
        }
    }

    public static void mouseHoverAndClickElement(By locator) {
        WebElement element = driver.findElement(locator);
        waitForElementEnabled(locator);
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().perform();
    }

    public static void waitForElementEnabled(By locator) {
        WebElement element = driver.findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(d -> element.isEnabled());
    }

    public static void clickAcceptCookiesButton() {
        driver.findElement(acceptCookies).click();
    }

    public static By getLocator(LocatorType locatorType, String value) {
        switch (locatorType) {
            case ID:
                return By.id(value);
            case NAME:
                return By.name(value);
            case CLASS_NAME:
                return By.className(value);
            case TAG_NAME:
                return By.tagName(value);
            case CSS_SELECTOR:
                return By.cssSelector(value);
            case XPATH:
                return By.xpath(value);
            default:
                throw new IllegalArgumentException("Unknown locator type: " + locatorType);
        }
    }
}


