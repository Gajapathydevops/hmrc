package pages;

import org.hamcrest.core.StringContains;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.*;

public class LoginPage extends BaseClass {

    By signIn = getLocator(LocatorType.XPATH, "//a/span[contains(text(),'Sign in')]");
    By gatewayId = By.id("user_id");
    By password = By.id("password");
    By submit = By.id("continue");
    By errorMsg = By.cssSelector("#user_id-error");
    By Tariffs = By.cssSelector("(//nav/li)[1]");
    String governmentActivity = "//ul[contains(@class,'government-activity')]//a[@data-track-dimension='LINK']";


    public void clickSignIn() {
        mouseHoverAndClickElement(signIn);
    }

    public void enterSignDetails(String gId, String pwd) {
        driver.findElement(gatewayId).sendKeys(gId);
        driver.findElement(password).sendKeys(pwd);
    }

    public void signInSubmit() {
        mouseHoverAndClickElement(submit);
    }

    public void validateErrorMessage() {
        String text = driver.findElement(errorMsg).getText();
        Assert.assertThat("Error:\n" +
                        "Enter a valid user ID and password. You will be locked out for 2 hours if you enter the wrong details 5 times",
                StringContains.containsString("Enter a valid user ID and password"));
    }

    public void collectionListExample() {
        List<WebElement> listElements = driver.findElements(By.xpath("//div[@class='govuk-grid-row gem-c-layout-super-navigation-header__navigation-items']/div//a"));
        List<String> storeList = new ArrayList<>();

        // Use the forEach method to iterate over the list and print the text of each link
        listElements.forEach(link -> System.out.println("forEach links ===> " + link.getText()));

        for (WebElement listValues : listElements) {
            storeList.add(listValues.getText());
        }
        for (String listOfLinks : storeList) {
            System.out.println("LIST LINKS ->  " + listOfLinks);
        }
        if (storeList.contains("Departments")) {
            WebElement element = driver.findElement(By.xpath(governmentActivity.toString().replace("LINK", "Departments")));
            element.click();
        }
    }

    public void collectionSetExample() {
        List<WebElement> setElements = driver.findElements(By.xpath("//div[@class='govuk-grid-row gem-c-layout-super-navigation-header__navigation-items']/div//a"));
        Set<String> setLinks = new HashSet<>();
        for (WebElement ele : setElements) {
            setLinks.add(ele.getText());
        }
        for (String setOfLinks : setLinks) {
            System.out.println("SET LINKS ->  " + setOfLinks);
        }
    }

    public void     collectionMapExample() {
        List<WebElement> mapElements = driver.findElements(By.xpath("//div[@class='govuk-grid-row gem-c-layout-super-navigation-header__navigation-items']/div//a"));
        Map<String, String> mapEntry = new HashMap<>();
        for (WebElement mapEle : mapElements) {
            mapEntry.put(mapEle.getText(), mapEle.getAttribute("href"));
        }
        for (Map.Entry<String, String> entry : mapEntry.entrySet()) {
            System.out.println("KEY -> " + entry.getKey() + "  VALUE ->" + entry.getValue());
        }
    }
}
