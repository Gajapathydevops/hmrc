@smoke
Feature: Login application

  Background: Launch the application
    Given open the application

  Scenario Outline: User login to application with invalid data
    When click on sing in button
    And enter the "<gatewayId>" and "<password>"
    Then validate the error message
    And close the application
    Examples:
      | gatewayId | password   |
      | 1234      | abcdefghij |