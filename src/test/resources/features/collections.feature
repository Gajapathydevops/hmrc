Feature: Collection examples in selenium

  Scenario: Validate the Collection List examples in selenium
    Given open the application
    When user click on menu
    And perform collection LIST examples
    And close the application

  Scenario: Validate the Collection Set examples in selenium
    Given open the application
    When user click on menu
    And perform collection SET examples
    And close the application

  Scenario: Validate the Collection Map examples in selenium
    Given open the application
    When user click on menu
    And perform collection MAP examples
    And close the application